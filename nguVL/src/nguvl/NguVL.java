/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nguvl;

/**
 *
 * @author Nguyen Son
 */
public class NguVL {

   private int a,b;

    public NguVL(int a, int b) {
        this.a = a;
        this.b = b;
    }

   
    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }
   public int cong(){
       return getA()+getB();
   }
   public int tru(){
       return getA()-getB();
   }
   public int nhan(){
      return getA()*getB();
   }
   public float chia(){
       return (float)getA()/getB();
   }
    public static int USCLN(int a, int b) {
        if (b == 0) return a;
        return USCLN(b, a % b);
    }
}
