/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demo;

/**
 *
 * @author Nguyen Son
 */
public class Demo2 implements Comparable<Demo3>{
    
    private String firstName;
    private String lastName;

    
    public Demo2(String firstName,String lastName)
    {
        this.firstName=firstName;
        this.lastName=lastName;
    }
    @Override
    public int compareTo(Demo3 abc)
    {
        //so sanh 2 string
       int value = this.lastName.compareTo(abc.lastName);
       
       //neu ho cua 2 doi tuong la ko bang nhau
        if(value!=0){
            return value;
            
        //neu ho cua 2 doi tuog bang nhau 
        //so sanh ten
        value=this.firstName.compareTo(abc.firstName);
        return value;
        }
    
    }
    
}
